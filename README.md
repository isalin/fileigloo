# README #

File-Igloo is a semi-anonymous filesharing site designed around letting its users quickly and easily upload and comment on various files. 


### How do I get set up? ###

The following instructions assume that both node.js and npm are installed.

In order to get the website working, you will need to download the repository in its current state. One way of doing this is through the terminal:

	
```
#!bash

	git clone https://bitbucket.org/isalin/fileigloo.git
```


You can then automatically install the project dependencies listed in “package.json” by navigating to the project directory and running:


```
#!bash

	npm install

```

Then finally launch the server by running:


```
#!bash

	node server.js

```

Things you might want to consider is changing the maximum accepted filesize (default is 1MB) and the upload directory (default is a subdirectory “/uploads”). 

The site should now be accessible via http://localhost:8080/ (alternatively http://127.0.0.1:8080/).

By default, the first user created is considered the administrator.