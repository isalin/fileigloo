var http = require('http'),
express = require('express'),
app = express(),
sqlite3 = require('sqlite3').verbose(),
db = new sqlite3.Database('database.db');

var session = require('express-session');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var crypto = require('crypto'); //Only used to generate random filenames when storing to disk

var bcrypt = require('bcrypt');

var path = require('path');
var fs = require('fs-extra');    //File System-needed for renaming file etc

app.set('views', __dirname + '/www');
app.engine('.html', require('jade').__express);

var multer = require('multer');
var storage = multer.diskStorage({
  destination: './uploads/',
  filename: function (req, file, cb) {
    crypto.pseudoRandomBytes(16, function (err, raw) {
      if (err) return cb(err)

      cb(null, raw.toString('hex') + path.extname(file.originalname))
    })
  }
})
//app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)

app.set('view engine', 'jade');

app.use(cookieParser());
app.use(session({
  cookie: {
    path    : '/',
    httpOnly: false,
    maxAge  : 24*60*60*1000
  },
  secret: 'supersecret'
}));
//app.engine('html', require('ejs').renderFile);
//app.set('view engine', 'html');



app.use(multer({dest:'./uploads/', storage: storage, limits: { fileSize: 1024*1024 }}).single('upload'));

//Static path
app.use(express.static(path.join(__dirname, './www')));

// Enable POST requests
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());


function checkAuth(req, res, next) {
  if (!req.session.user_id) {
    res.send('You are not authorized to view this page');
  } else {
    next();
  }
}

app.get('/my_secret_page', checkAuth, function (req, res) {
  res.send('if you are viewing this page it means you are logged in');
});

app.get('/login', function (req, res) {
  if (!req.session.user_id) {
    if (typeof req.query.error === 'undefined'){
      res.render('login.jade', {}, function(err, html) {
        res.status(200).send(html);
      });
    } else {
      res.render('login.jade', {error: true}, function(err, html) {
        res.status(200).send(html);
      });
    }
  } else {
    res.redirect('/');
  }
});

app.get('/signup', function (req, res) {
  if (!req.session.user_id) {
    if (typeof req.query.usernameerror != 'undefined'){
      console.log("Username taken!");
      res.render('signup.jade', {usernameerror:true}, function(err, html) {
        res.status(200).send(html);
      });
      return;
    }
    if (typeof req.query.usernameblank != 'undefined'){
      console.log("Username blank!");
      res.render('signup.jade', {usernameblank:true}, function(err, html) {
        res.status(200).send(html);
      });
      return;
    }
    if (typeof req.query.passworderror != 'undefined'){
      console.log("Blank password!");
      res.render('signup.jade', {passworderror:true}, function(err, html) {
        res.status(200).send(html);
      });
      return;
    }
    res.render('signup.jade', {}, function(err, html) {
      res.status(200).send(html);
    });
  } else {
    res.redirect('/');
  }
});

app.post('/signup', function(req, res) {
  console.log("Signup username: " + req.body.user);
  console.log("Signup Password: " + req.body.password);

  db.get("SELECT * FROM users WHERE username='" + req.body.user + "';", function(err, rows) {
    if (typeof rows != 'undefined'){
      res.redirect("/signup?usernameerror");
      return;
    } else if (req.body.password === ''){
      res.redirect("/signup?passworderror");
      return;
    } else if (req.body.user === ''){
      res.redirect("/signup?usernameblank");
      return;
    }


    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(req.body.password, salt, function(err, hash) {
        // Store hash in your password DB.
        sqlRequest = "INSERT INTO 'users' (username, password) VALUES('" + req.body.user + "', '" + hash + "')";
        console.log("SQL: " + sqlRequest);

        db.run(sqlRequest, function(err) {
          if(err !== null) {
            res.status(500).send("An error has occurred -- " + err);
          }
          else {
            db.serialize(function() {
              db.get("SELECT * FROM users WHERE username='" + req.body.user + "' AND password='" + hash + "';", function(err, rows) {
                console.log("Login id set to: " + rows.id);
                req.session.user_id = rows.id;
                req.session.username = rows.username;
                res.redirect("/");
              });
            });
          }
        });
      });
    });

  });
});

app.post('/login', function(req, res2) {
  console.log("Username: " + req.body.user);
  console.log("Password: " + req.body.password);
  db.serialize(function() {
    db.get("SELECT * FROM users WHERE username='" + req.body.user + "';", function(err, rows) {
      if (typeof rows === 'undefined'){
        res2.redirect("/login?error=0");
        return;
      }
      bcrypt.compare(req.body.password, rows.password, function(err, res) {
        console.log("Login successful = " + res);
        if(res){
          req.session.user_id = rows.id;
          res2.redirect("/login");
        } else {
          res2.redirect("/login?error=0");
        }
      });

    });
  });
});

//req.session.user_id = 1;
//res.redirect('/');
//res.send('Bad user/pass');

app.get('/logout', function (req, res) {
  delete req.session.user_id;
  res.redirect('/login');
});

app.get('/', function (req, res) {
  if (req.session.user_id) {
    var loggedIn = true;
  } else {
    var loggedIn = false;
  }
  db.serialize(function() {
    db.all("SELECT * FROM comments", function(err, rows) {
      var coms = rows;

      db.all('SELECT * FROM files WHERE uploadDate > ' + (new Date().getTime() - 86400000) + ' ORDER BY uploadDate DESC', function(err, row) {
        if(err !== null) {
          res.status(500).send("An error has occurred -- " + err);
        }
        else {
          res.render('index.jade', {files: row, loggedIn: loggedIn, comments: coms, pageData: {username: [req.session.user_id]}}, function(err, html) {
            res.status(200).send(html);
          });
        }
      });
    });
  });
});

app.get('/profile', function (req, res) {
  if (req.session.user_id) {
    var loggedIn = true;
  } else {
    var loggedIn = false;
  }
  var user = "place";
  db.serialize(function() {
    db.get("SELECT * FROM users WHERE id='" + req.session.user_id + "';", function(err, rows) {
      user = rows.username;
      console.log("Displaying profile of: " + user);

      req.session.user_id = rows.id;
    });
    db.all('SELECT * FROM files WHERE user_id="' + req.session.user_id + '" ORDER BY uploadDate DESC', function(err, row) {
      if(err !== null) {
        res.status(500).send("An error has occurred -- " + err);
      }
      else {
        res.render('profile.jade', {files: row, loggedIn: loggedIn, pageData: {username: [user]}}, function(err, html) {
          res.status(200).send(html);
        });
      }
    });
  });

});
app.get('/upvote/:fild_id', function(req, res) {
  sqlRequest = "UPDATE files SET score = RevisionId + 1 WHERE id='" + req.params.file_id + "'"
  db.run(sqlRequest, function(err) {
    if(err !== null) {
      res.status(500).send("An error has occurred -- " + err);
    } else {
      res.redirect('back');
    }
  });});

  app.get('/upload', function(req, res) {
    if (req.session.user_id) {
      var loggedIn = true;
    } else {
      var loggedIn = false;
    }
    res.render('uploadTest.jade', {loggedIn: loggedIn}, function(err, html) {
      res.status(200).send(html);
    });

  });
  //Lists top files by day/week/month/year
  app.get('/top', function(req, res) {
    if (req.session.user_id) {
      var loggedIn = true;
    } else {
      var loggedIn = false;
    }
    db.all("SELECT * FROM comments", function(err, rows) {
      coms = rows;
    });


    if (typeof req.query.time === 'undefined'){
      db.all('SELECT * FROM files ORDER BY score DESC', function(err, row) {
        if(err !== null) {
          res.status(500).send("An error has occurred -- " + err);
        }
        else {
          res.render('index.jade', {files: row, loggedIn: loggedIn, comments: coms, pageData: {username: [req.session.user_id]}}, function(err, html) {
            res.status(200).send(html);
          });
        }
      });
    } else if (req.query.time === 'day'){
      console.log("Listing by day...");
      db.all('SELECT * FROM files WHERE uploadDate > ' + (new Date().getTime() - 86400000) + ' ORDER BY score DESC', function(err, row) {
        if(err !== null) {
          res.status(500).send("An error has occurred -- " + err);
        }
        else {
          res.render('index.jade', {files: row, loggedIn: loggedIn, comments: coms, pageData: {username: [req.session.user_id]}}, function(err, html) {
            res.status(200).send(html);
          });
        }
      });
    } else if (req.query.time === 'week'){
      console.log("Listing by day...");
      db.all('SELECT * FROM files WHERE uploadDate > ' + (new Date().getTime() - 86400000*7) + ' ORDER BY score DESC', function(err, row) {
        if(err !== null) {
          res.status(500).send("An error has occurred -- " + err);
        }
        else {
          res.render('index.jade', {files: row, loggedIn: loggedIn, comments: coms, pageData: {username: [req.session.user_id]}}, function(err, html) {
            res.status(200).send(html);
          });
        }
      });
    } else if (req.query.time === 'month'){
      console.log("Listing by day...");
      db.all('SELECT * FROM files WHERE uploadDate > ' + (new Date().getTime() - 86400000*30) + ' ORDER BY score DESC', function(err, row) {
        if(err !== null) {
          res.status(500).send("An error has occurred -- " + err);
        }
        else {
          res.render('index.jade', {files: row, loggedIn: loggedIn, comments: coms, pageData: {username: [req.session.user_id]}}, function(err, html) {
            res.status(200).send(html);
          });
        }
      });
    } else if (req.query.time === 'year'){
      console.log("Listing by day...");
      db.all('SELECT * FROM files WHERE uploadDate > ' + (new Date().getTime() - 86400000*365) + ' ORDER BY score DESC', function(err, row) {
        if(err !== null) {
          res.status(500).send("An error has occurred -- " + err);
        }
        else {
          res.render('index.jade', {files: row, loggedIn: loggedIn, comments: coms, pageData: {username: [req.session.user_id]}}, function(err, html) {
            res.status(200).send(html);
          });
        }
      });
    }
  });

  app.get('/files/:name', function(req, res) {
    res.sendFile(__dirname + '/uploads/' + req.params.name);
  });

  app.get('/upload', function(req, res) {
    if (req.session.user_id) {
      var loggedIn = true;
    } else {
      var loggedIn = false;
    }
    res.render('uploadTest.jade', {loggedIn: loggedIn}, function(err, html) {
      res.status(200).send(html);
    });

  });

  app.post('/upload', function(req, res) {
    if (req.session.user_id) {
      var loggedIn = true;
    } else {
      var loggedIn = false;
      res.status(500).send("An error has occurred -- Please log in again");
      return;
    }
    title = req.body.title;
    user_id = req.session.user_id;
    comment_text = req.body.comment_text;
    filename = req.file.filename
    console.log(req.file.filename);
    sqlRequest = "INSERT INTO 'files' (title, filename, user_id, uploadDate) VALUES('" + title + "', '" + filename + "', '" + user_id + "', '" + new Date().getTime() + "')"
    db.run(sqlRequest, function(err) {
      if(err !== null) {
        res.status(500).send("An error has occurred -- " + err);
      } else {
        res.redirect('/');
      }
    });

  });

  //Get comments
  app.get('/comments/:id', function(req, res) {
    if (req.session.user_id) {
      var loggedIn = true;
    } else {
      var loggedIn = false;
    }
    sqlRequest = "UPDATE files SET score = score + 1 WHERE id='" + req.params.id + "'"
    db.run(sqlRequest, function(err) {
      if(err !== null) {
        res.status(500).send("An error has occurred -- " + err);
      } else {

      }
    });

    db.all("SELECT * FROM files WHERE id='" + req.params.id + "'", function(error, rows) {
      temp1 = rows;
    })
    db.all("SELECT * FROM users", function(error, rows) {
      temp2 = rows;
    })
    db.all("SELECT * FROM comments WHERE file_id='" + req.params.id + "'", function(err, row) {
      if(err !== null) {
        res.status(500).send("An error has occurred -- " + err);
      }
      else {
        for (var i = 0; i < row.length; i++) {
          console.log('Comment: ', row[i].comment_text);
        }
        res.render('comments.jade', {comments: row, file: temp1, usernames: temp2, loggedIn: loggedIn, pageData: {id: [req.session.user_id]}}, function(err, html) {
          res.status(200).send(html);
        });
      }
    });
  });


  app.get('/comments/delete/:id', function(req, res) {
    db.run("DELETE FROM comments WHERE id='" + req.params.id + "'", function(err) {
      if(err !== null) {
        res.status(500).send("An error has occurred -- " + err);
      }
      else {
        res.redirect('back');
      }
    });
  });

  // Add comment
  app.post('/comments/add', function(req, res) {
    file_id = req.body.file_id;
    user_id = req.session.user_id;
    comment_text = req.body.comment_text;

    sqlRequest = "INSERT INTO 'comments' (file_id, user_id, comment_text) VALUES('" + file_id + "', '" + user_id + "', '" + comment_text + "')"
    db.run(sqlRequest, function(err) {
      if(err !== null) {
        res.status(500).send("An error has occurred -- " + err);
      }
      else {
        sqlRequest = "UPDATE files SET score = score + 5 WHERE id='" + req.params.id + "'"
        db.run(sqlRequest, function(err) {
          if(err !== null) {
            res.status(500).send("An error has occurred -- " + err);
          } else {
            res.redirect('back');
          }
        });
      }
    });
  });

  app.post('/add', function(req, res) {
    title = req.body.title;
    file_path = req.body.file_path;
    sqlRequest = "INSERT INTO 'files' (title, file_path) VALUES('" + title + "', '" + file_path + "')"
    db.run(sqlRequest, function(err) {
      if(err !== null) {
        res.status(500).send("An error has occurred -- " + err);
      }
      else {
        res.redirect('back');
      }
    });
  });

  app.get('/delete/:id', function(req, res) {
    db.run("DELETE FROM files WHERE id='" + req.params.id + "'", function(err) {
      if(err !== null) {
        res.status(500).send("An error has occurred -- " + err);
      }
      else {
        res.redirect('back');
      }
    });
  });




  //API
  app.get('/api', function(req, res) {
    if (req.session.user_id) {
      var loggedIn = true;
    } else {
      var loggedIn = false;
    }
    res.render('api.jade', {loggedIn: loggedIn}, function(err, html) {
      res.status(200).send(html);
    });
  });

  //Get top files
  app.get('/api/getFiles', function(req, res) {
    if(req.query.test == "new"){
      db.all('SELECT * FROM files ORDER BY id DESC', function(err, row) {
        if(err !== null) {
          res.status(500).send("An error has occurred -- " + err);
        }
        else {
          res.render('./api/getFiles.jade', {files: row, loggedIn: loggedIn, comments: coms, pageData: {username: [req.session.user_id]}}, function(err, html) {
            res.status(200).send(html);
          });
        }
      });
    } else if(req.query.test == "top"){
      db.all('SELECT * FROM files ORDER BY score DESC', function(err, row) {
        if(err !== null) {
          res.status(500).send("An error has occurred -- " + err);
        }
        else {
          res.render('./api/getFiles.jade', {files: row, loggedIn: loggedIn, comments: coms, pageData: {username: [req.session.user_id]}}, function(err, html) {
            res.status(200).send(html);
          });
        }
      });
    }
  });

  app.get('/api/download', function(req, res) {
    db.serialize(function() {
      db.get("SELECT * FROM files WHERE id='" + req.query.id + "';", function(err, rows) {
        res.redirect("/files/" + rows.filename);
      });
    });
  });

  app.get('/upload', function(req, res) {

    res.render('uploadTest.jade', function(err, html) {
      res.status(200).send(html);
    });

  });

  var port = process.env.PORT || 8080;
  var host = process.env.HOST || "127.0.0.1";

  // Start the server
  var server = http.createServer(app).listen(port, host, function() {
    console.log("Server listening to %s:%d within %s environment",
    host, port, app.get('env'));
  });
